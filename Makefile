# Presentation
presentation: presentation.pdf presentation_notes.pdf
presentation.pdf: presentation.tex slides.tex \
	$(CC_IMAGES) \
	$(IMAGES)
presentation_notes.pdf: presentation_notes.tex slides.tex \
	$(CC_IMAGES) \
	$(IMAGES)

CC_IMAGES = \
	cc_by_30.pdf \
	cc_nc_30.pdf \
	cc_sa_30.pdf \
	$(NULL)
IMAGES = \
	$(NULL)

.PHONY: presentation

# Make PDFs from TeX files
# The --shell-escape argument is necessary to get dot2tex to work
PDFLATEX = pdflatex --shell-escape

%.pdf: %.tex
	$(PDFLATEX) $(@:.pdf=.tex) && $(PDFLATEX) $(@:.pdf=.tex)

# Clean
clean:
	rm -f presentation.aux presentation.log presentation.nav presentation.out presentation.pdf presentation.snm presentation.toc presentation.vrb \
	      presentation_notes.log presentation_notes.pdf presentation_notes.vrb guadec-theme.aux \
	      missfont.log
.PHONY: clean
