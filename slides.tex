\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{hyperref}
\hypersetup{bookmarks=true, colorlinks=false, linkcolor=blue, citecolor=blue, filecolor=blue, pagecolor=blue, urlcolor=blue,
            pdftitle=What’s Coverity static analysis ever done for us?,
            pdfauthor=Philip Withnall, pdfsubject=, pdfkeywords=}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,automata,positioning,er,calc,decorations.pathmorphing,fit}
\usepackage{listings}
\usepackage{subfigure} % can't use subfig because it doesn't support Beamer
\usepackage{fancyvrb}
\usepackage{ulem}
\usepackage{siunitx}

% Syntax highlighting colours from the Tango palette: http://pbunge.crimson.ch/2008/12/tango-syntax-highlighting/
\definecolor{LightButter}{rgb}{0.98,0.91,0.31}
\definecolor{LightOrange}{rgb}{0.98,0.68,0.24}
\definecolor{LightChocolate}{rgb}{0.91,0.72,0.43}
\definecolor{LightChameleon}{rgb}{0.54,0.88,0.20}
\definecolor{LightSkyBlue}{rgb}{0.45,0.62,0.81}
\definecolor{LightPlum}{rgb}{0.68,0.50,0.66}
\definecolor{LightScarletRed}{rgb}{0.93,0.16,0.16}
\definecolor{Butter}{rgb}{0.93,0.86,0.25}
\definecolor{Orange}{rgb}{0.96,0.47,0.00}
\definecolor{Chocolate}{rgb}{0.75,0.49,0.07}
\definecolor{Chameleon}{rgb}{0.45,0.82,0.09}
\definecolor{SkyBlue}{rgb}{0.20,0.39,0.64}
\definecolor{Plum}{rgb}{0.46,0.31,0.48}
\definecolor{ScarletRed}{rgb}{0.80,0.00,0.00}
\definecolor{DarkButter}{rgb}{0.77,0.62,0.00}
\definecolor{DarkOrange}{rgb}{0.80,0.36,0.00}
\definecolor{DarkChocolate}{rgb}{0.56,0.35,0.01}
\definecolor{DarkChameleon}{rgb}{0.30,0.60,0.02}
\definecolor{DarkSkyBlue}{rgb}{0.12,0.29,0.53}
\definecolor{DarkPlum}{rgb}{0.36,0.21,0.40}
\definecolor{DarkScarletRed}{rgb}{0.64,0.00,0.00}
\definecolor{Aluminium1}{rgb}{0.93,0.93,0.92}
\definecolor{Aluminium2}{rgb}{0.82,0.84,0.81}
\definecolor{Aluminium3}{rgb}{0.73,0.74,0.71}
\definecolor{Aluminium4}{rgb}{0.53,0.54,0.52}
\definecolor{Aluminium5}{rgb}{0.33,0.34,0.32}
\definecolor{Aluminium6}{rgb}{0.18,0.20,0.21}

\usetheme{guadec}

\AtBeginSection{\frame{\sectionpage}}

% Abstract here: https://2017.guadec.org/talks-and-events/#abstract-22-whats_coverity_static_analysis_ever_done_for_us
\title{What’s Coverity static analysis ever done for us?}

\author{Philip Withnall\\Endless Mobile\\\texttt{philip@tecnocode.co.uk}}
\date{July 30, 2017}

\begin{document}


\begin{frame}
\titlepage
\end{frame}

\note{25 minutes allocated. 15 minutes for talk, 10 minutes for questions.}


\begin{frame}{What is static analysis?}
Compile-time testing of all possible code paths.
\end{frame}

\note{Static analysis is one way of deriving information about code. It’s normally contrasted with dynamic analysis, i.e.\ actually running the code.}
\note{Most static analyses look at all possible control paths through the code and check properties of each of those control paths. So, for example, they will check all branches of an if-block, or the taken, taken-once, taken-twice, taken-n-times iterations of a loop.}
\note{This makes static analysis complex and slow, although that is not always true. Compilers do some cheap static analyses during compilation (for example, finding dead code). Typically, though, the term ‘static analysis’ is used to refer to more complex analyses which are run separately from compilation.}


\begin{frame}{What is Coverity Scan?}
\begin{itemize}
        \item{Proprietary}
        \item{Free to use for open source projects}
        \item{A locally run tool and paired web service}
\end{itemize}
\end{frame}

\note{Coverity Scan is a product produced by Coverity, which is an offshoot from Stanford university. It’s a proprietary web service and compiler interposition which is free to use for open source projects (and costs a lot for proprietary projects).}
\note{You run a special \texttt{cov-build} tool which wraps your compiler inside \texttt{make} and which produces an analysis file to upload to the web service. The web service performs the analysis and displays the results.}


\begin{frame}{What is Coverity Scan?}
\includegraphics[keepaspectratio=true,width=\textwidth]{coverity-overview.png}
\end{frame}

\note{A screenshot of the overview page provided by the web service.}


\begin{frame}{What is Coverity Scan?}
\includegraphics[keepaspectratio=true,width=\textwidth]{coverity-issue.png}
\end{frame}

\note{A screenshot of some scan results. They include the code which was analysed, and give line-by-line indications of which control paths were taken to reach the error state found.}
\note{I’ll go through this screen in more detail later.}


\begin{frame}{Is it the best tool for the job?}
\begin{itemize}
        \item{Mature support for triaging and dismissing false positives}
        \item{Wide use over many projects and active development}
        \item{Free to use}
        \item{Proprietary}
        \item{Submission rate limiting}
        \item{Should be used as one tool out of many}
\end{itemize}
\end{frame}

\note{Is it the best tool for the job? It’s one tool out of many, but it is one of the best tools out of many. It’s a mature product which has been run on millions (billions?) of lines of code.}
\note{It’s actively supported by a large company, who presumably have an interest in using open source projects as a test bed or source of ground truth for their analyses. (In the sense that if open source projects are trying out Coverity and reporting problems, that means fewer problems for their customers to report. This is all supposition.)}
\note{But what job is it best for? Since it’s freeware to us, submissions for scanning are rate limited, so we can’t reasonably use it as a try-server for all commits. Currently, we’re running it once a week on various projects, which seems like a reasonable compromise for rate limiting. However, it does mean that error reports can come in a week after the change which caused them.}
\note{One of the common complaints against static analysis is the number of false positives it produces. Coverity includes two good mechanisms to mitigate this (and, they claim, get the false positive rate down to around 10\% of reports for a project): dismissal of reports, and custom modelling files.}
\note{False positive reports can be dismissed easily from the web interface, and they do not reappear in subsequent scans, even if the code around that report changes slightly. This is a surprisingly reliable system.}
\note{Modelling files are a little more complex, but if you’re interested in them I can discuss them after the talk.}


\begin{frame}{How have we been using Coverity?}
\begin{itemize}
        \item{Jenkins + JHBuild}
        \item{Manually created Jenkins jobs}
        \item{Limited set of hand-picked ‘security critical’ modules}
        \item{E-mail notification of scan results}
        \item{Partial ownership by module maintainers}
        \item{No real comaintainership of the project}
\end{itemize}
\end{frame}

\note{We’ve been running Coverity scans from jenkins.freedesktop.org for a limited set of security critical modules, running a scan once a week for each module, for several months.}
\note{The scan results get e-mailed to me and the module maintainers. One of the downsides of this is that an e-mail gets sent out even if the results don’t change week-on-week (for example, no new bugs, no bugs eliminated).}
\note{The level of ownership of the Coverity results by module maintainers varies: some maintainers actively work on the Coverity issues and take ownership of their own module’s score. Some of them look on passively and accept patches for eliminating bugs. Some of them ignore things entirely, but have allowed their modules to be set up in Coverity.}
\note{Registering a module with Coverity needs to be done by the maintainer or a contributor, as Coverity manually verify all new module submissions (both to check if they’re open source, and to ensure they are being submitted by a project maintainer). Limiting module ownership to maintainers and contributors means Coverity isn’t handing CVEs to malicious people.}
\note{The overall Jenkins architecture is maintained by me and, when I frustrate him enough, Daniel Stone (thanks Daniel). It would be good to get some comaintainership here, and I have ideas for how to make this easier.}


\begin{frame}{What impact has this had?}
\begin{center}
        \includegraphics[keepaspectratio=true,width=0.5\textwidth]{decline.png}
        \vspace{0.5em}
        \\
        \small{Randall Munroe, \url{https://xkcd.com/523/}, CC-BY-NC 2.5}
\end{center}
\end{frame}

\note{The number of issues has come down, as reported by the graphs in Coverity.}
\note{The number of issues which were high-impact true positives is harder to determine. Anecdotally, a few medium or high-impact true positives have been found per module.}
\note{Special thanks to Richard Hughes, Timm Baeder, Bastien Nocera, Tobias Mueller, who have all embraced the Coverity reports and used them in their normal development workflow.}


\begin{frame}{How is this useful?}
\begin{itemize}
        \item{Find bugs in error paths}
        \item{Complements unit testing}
        \item{Find bugs in parsers and file loaders}
        \item{Find bugs before they are hit at runtime}
        \item{Jenkins won’t forget to run analyses like maintainers do}
\end{itemize}
\end{frame}

\note{The first question to ask is: what are you trying to achieve? Generally, we’re trying to achieve improved code quality by fixing bugs. Hence, we want static analyses which return true positives, are easy to use, and can be run regularly.}
\note{Coverity fulfils all those criteria. Other static analysis tools do, but generally not quite as well. Coverity is not the only tool you need to use, however. A good approach to ensuring code quality is to run all the available tools (\texttt{clang-analyzer}, Coverity, asan, tsan, ubsan, etc.) on it, and to run them all regularly. They all find complementary (and sometimes overlapping) sets of problems.}
\note{Static analysis tools run particularly well on tight code sections which developers find hard to reason about: parsers and file loaders. They work on other bits of code too, but generally return more true positives for parsing code.}
\note{The bugs which static analysis tools find are not linked to bug reports --- by fixing a Coverity report, you can’t immediately see that you’ve fixed a crasher bug which affects hundreds of people. However, you can end up fixing bugs before people ever hit them at runtime --- this works particularly well when all the initial reports have been triaged, and Coverity is just being used to analyse new code pushed to git.}
\note{By running the analysis regularly using Jenkins, maintainers don’t need to remember to do it, and can’t put it off because they’re rushing for a release. This is a form of CI pipeline, and has the advantages of that.}


\begin{frame}{How is this not useful?}
\begin{itemize}
        \item{Not reasonable to use as a try-server}
        \item{Initial dump of false positives when adding a project}
        \item{Problems with handling idiomatic C}
        \item{Jenkins + JHBuild is not the most reliable}
\end{itemize}
\end{frame}

\note{While it’s a form of CI pipeline, we can’t easily use Coverity to gate contributions to modules (a try-server, running Coverity on each patch before it’s pushed to git master), as it rate limits submissions.}
\note{When a project is initially added to Coverity, a lot of reports will be generated for the existing code, and these need to be triaged. This is a pain, but necessary to remove noise from the reports, establish various approaches to avoiding similar false positives in future (for example, a model file for Coverity; see its documentation), and fix any endemic problems in the module. (For example, any coding anti-patterns in the module which should be fixed architecturally.)}
\note{The more runtime assumptions are encoded in code, the more false positives a module is going to get from Coverity. The way to fix this is to make those assumptions more explicit in the code, by adding runtime assertions. Coverity understands \lstinline{assert()} and \lstinline{g_assert()}, and will use the conditions from them to eliminate false positives.}
\note{The combination of Jenkins and JHBuild is not the most reliable. I may change the architecture for how we submit Coverity scans in future.}


\begin{frame}{How do I get involved?}
\begin{itemize}
        \item{Talk to me; propose modules for inclusion into Jenkins}
        \item{Or go with Coverity yourself}
        \item{Or try other static analysis tools (\texttt{clang-analyzer}?) and let me know!}
\end{itemize}
\end{frame}

\note{If you want your module to be included in the set of modules built by Jenkins, let me know and we can get it set up. The only prerequisite is that your module’s buildable using JHBuild (and is written in C; although Coverity does claim to support other languages; we haven’t tried that yet).}
\note{There’s nothing stopping you using Coverity independently, either. Just register your project on scan.coverity.com and submit a scan for analysis.}
\note{Or, you could experiment with running other analysis tools, like \texttt{clang-analyzer} or (dynamic tools) asan, tsan and ubsan. Give them a go and see what happens.}


\begin{frame}{Miscellany}
\begin{description}
	\item[Jenkins jobs]{\url{https://jenkins.freedesktop.org/view/GNOME\%20Coverity/}}
	\item[Coverity]{\url{http://scan.coverity.com/}}
	\item[Wikipedia on static analysis]{\url{https://en.wikipedia.org/wiki/Static_program_analysis}}
\end{description}

% Creative commons logos from http://blog.hartwork.org/?p=52
\vfill
\begin{center}
	\includegraphics[scale=0.83]{cc_by_30.pdf}\hspace*{0.95ex}\includegraphics[scale=0.83]{cc_sa_30.pdf}\\[1.5ex]
	{\tiny\textit{Creative Commons Attribution-ShareAlike 4.0 International License}}\\[1.5ex]
	\vspace*{-2.5ex}
\end{center}

\vfill
\begin{center}
	\tiny{Beamer theme: \url{https://git.gnome.org/browse/presentation-templates/tree/GUADEC/2017}}
\end{center}
\end{frame}

\end{document}
